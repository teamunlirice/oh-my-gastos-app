//
//  ExpenseDisplayTableViewController.swift
//  OhMyGastos
//
//  Created by ThinkDWM on 4/12/15.
//  Copyright (c) 2015 ThinkDWM. All rights reserved.
//

import UIKit

class ExpenseDisplayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let pStorage = NSUserDefaults.standardUserDefaults()
    var mutableArray = NSMutableArray()
    /********************************************************************
    *
    * INITIALIZATION
    * this is where the initialization takes place.
    * view did load and other view did functions should be placed in
    * here.
    *
    *********************************************************************
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var client = ClientAPI()
        client.getExpenses { (response) -> Void! in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.mutableArray = (response as NSArray).mutableCopy() as NSMutableArray
                self.tableView.reloadData()
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mutableArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("expense", forIndexPath: indexPath) as ExpenseCell
        var data = self.mutableArray.objectAtIndex(indexPath.row) as NSDictionary
        var amount = data.objectForKey("amount") as Int
        cell.priceLabel.text = "P \(amount)"
        cell.descriptionLabel.text = data.objectForKey("name") as NSString
        cell.photoImageView.setImageWithURL(NSURL(string: data.objectForKey("image") as NSString), usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        cell.avatarImage.setImageWithURL(NSURL(string: data.objectForKey("creator_image") as NSString), usingActivityIndicatorStyle: .Gray)
        cell.spentByLabel.text = "Spent by " + (data.objectForKey("creator") as NSString)
        return cell
    }
    
    /********************************************************************
    *
    * ACTION FUNCTIONS.
    * this is where the action functions for writing codes are written
    * and places.
    *
    *********************************************************************
    */
    
    /********************************************************************
    *
    * UI MANIPULATION FUNCTIONS.
    * this is where the ui manipulation functions for writing codes are
    * written and places.
    *
    *********************************************************************
    */
}