//
//  AddExpenseViewController.swift
//  OhMyGastos
//
//  Created by ThinkDWM on 4/12/15.
//  Copyright (c) 2015 ThinkDWM. All rights reserved.
//

class AddExpenseViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let pStorage = NSUserDefaults.standardUserDefaults()
    let picker = UIImagePickerController()
    @IBOutlet weak var expenseTextField: UITextField!
    @IBOutlet weak var expenseTypeTextField: UITextField!
    @IBOutlet weak var picture: UIImageView!
    
    @IBOutlet weak var maskLoading: UIView!
    
    var AWS_ACCESS_KEY_ID: NSString { return "AKIAJJB4ANZTNMKBKBSQ" }
    var AWS_SECRET_ACCESS_KEY: NSString { return "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" }
    
    var photo = UIImage()
    var screenSize = UIScreen.mainScreen().bounds
    /********************************************************************
    *
    * INITIALIZATION
    * this is where the initialization takes place.
    * view did load and other view did functions should be placed in
    * here.
    *
    *********************************************************************
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /********************************************************************
    *
    * ACTION FUNCTIONS.
    * this is where the action functions for writing codes are written
    * and places.
    *
    *********************************************************************
    */
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        return false
    }
    
    @IBAction func saveAction(sender: AnyObject) {
        
        var client = ClientAPI()
        if let imageData: NSData = UIImageJPEGRepresentation(self.photo, 0.5) {
//            var loadingScreen = NSBundle.mainBundle().loadNibNamed("LoadingView", owner: self, options: nil) as NSArray
//            self.view.addSubview(loadingScreen[0] as UIView)
            maskLoading.hidden = false
            self.uploadImageToS3(imageData, completion: {(response) -> Void in
                var imageUrl = response as NSString
                client.createExpense(self.expenseTypeTextField.text, expense: self.expenseTextField.text, image: imageUrl) { (response) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.performSegueWithIdentifier("unwindToStartLand", sender: self)
                    })
                }
            })
        } else {
            var alertController = MISAlertController(title: "Error", message: "Please complete everything.", preferredStyle: .Alert)
            alertController.addAction(MISAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
            alertController.showInViewController(self, animated: true)
        }
    }
    
    @IBAction func addPhotoAction(sender: AnyObject) {
        self.picker.delegate = self
        self.picker.allowsEditing = true
        
        var alertController = MISAlertController(title: "Options", message: "Pick where to get your photo.", preferredStyle: .ActionSheet)
        
        let cameraAction = MISAlertAction(title: "From Camera", style: .Default, handler: {
            (action: MISAlertAction!) -> Void in
            self.picker.sourceType = UIImagePickerControllerSourceType.Camera
            self.picker.cameraCaptureMode = .Photo
            self.picker.showsCameraControls = true
            self.presentViewController(self.picker, animated: true, completion: nil)
        })
        let libraryAction = MISAlertAction(title: "From Library", style: .Default, handler: {
            (action: MISAlertAction!) -> Void in
            self.picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(self.picker, animated: true, completion: nil)
        })
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.showInViewController(self, animated: true)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerEditedImage] as UIImage
        self.photo = info[UIImagePickerControllerEditedImage] as UIImage
        
        var newSize = CGSizeMake(screenSize.width - 20, 250)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        chosenImage.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        
        var newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.photo = newImage
        self.picture.image = chosenImage
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /********************************************************************
    *
    * UI MANIPULATION FUNCTIONS.
    * this is where the ui manipulation functions for writing codes are
    * written and places.
    *
    *********************************************************************
    */
    
    
    func uploadImageToS3(imageData: NSData, completion: (response: AnyObject)->Void!) {
        
        let date = NSDate()
        
        var currentDateString = NSString(format: "%f", date.timeIntervalSinceReferenceDate)
        var accessToken = NSUserDefaults.standardUserDefaults().objectForKey("accessToken") as NSString
        var hashableString = accessToken + currentDateString
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: self.AWS_ACCESS_KEY_ID, secretKey: self.AWS_SECRET_ACCESS_KEY)
        let defaultServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        defaultServiceConfiguration.maxRetryCount = 5
//        AWSServiceManager.defaultServiceManager().setDefaultServiceConfiguration(defaultServiceConfiguration)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = defaultServiceConfiguration
        
        var path:NSString = NSTemporaryDirectory().stringByAppendingPathComponent(hashableString.md5ify + ".png")
        
        imageData.writeToFile(path, atomically: true)
        
        var url: NSURL = NSURL(fileURLWithPath: path)!
        // next we set up the S3 upload request manager
        var uploadRequest: AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        // set the bucket
        uploadRequest.bucket = "petrainbow-development"
        // I want this image to be public to anyone to view it so I'm setting it to Public Read
        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
        // set the image's name that will be used on the s3 server. I am also creating a folder to place the image in
        uploadRequest.key = "petprofiles/" + hashableString.md5ify + ".png"
        // set the content type
        uploadRequest.contentType = "image/png"
        // and finally set the body to the local file path
        uploadRequest.body = url;
        
        // we will track progress through an AWSNetworkingUploadProgressBlock
//        
//        uploadRequest.uploadProgress = ([unowned self](bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) in
//            
//            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
//                
//            })
//        }
    
        
        // now the upload request is set up we can creat the transfermanger, the credentials are already set up in the app delegate
        var transferManager:AWSS3TransferManager = AWSS3TransferManager.defaultS3TransferManager()
        // start the upload
//        var task = BFExecutor.mainThreadExecutor() as BFTask
        transferManager.upload(uploadRequest).continueWithBlock { (task: BFTask!) -> AnyObject! in
            completion(response: "https://s3.amazonaws.com/petrainbow-development/petprofiles/" + hashableString.md5ify + ".png")
            return "all done"
        }
//        transferManager.upload(uploadRequest).continueWithExecutor(BFExecutor.mainThreadExecutor(), withBlock:{ [weak self]
//            task -> AnyObject in
//            completion(response: "https://s3.amazonaws.com/petrainbow-development/petprofiles/" + hashableString.md5ify + ".png")
//            return "all done"
//        })
    }
    
    
    
}

