//
//  StartLandViewController.swift
//  OhMyGastos
//
//  Created by ThinkDWM on 4/12/15.
//  Copyright (c) 2015 ThinkDWM. All rights reserved.
//

import UIKit

class StartLandViewController: UIViewController {
    
    let pStorage = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var householdNameLabel: UILabel!
    @IBOutlet weak var spentLabel: UILabel!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var levelImageView: UIImageView!
    /********************************************************************
    *
    * INITIALIZATION
    * this is where the initialization takes place.
    * view did load and other view did functions should be placed in
    * here.
    *
    *********************************************************************
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        var client = ClientAPI()
        client.getHousehold { (response) -> Void in
            println(response)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                var spent = response.objectForKey("expenses") as Int
                var remaining = response.objectForKey("remaining") as Int
                self.householdNameLabel.text = response.objectForKey("household_name") as NSString + " household"
                self.remainingLabel.text = "P \(remaining)"
                self.spentLabel.text = "P \(spent)"
                self.levelImageView.image = UIImage(named: "level" + (response.objectForKey("level") as NSString))
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /********************************************************************
    *
    * ACTION FUNCTIONS.
    * this is where the action functions for writing codes are written
    * and places.
    *
    *********************************************************************
    */
    @IBAction func logout(sender: AnyObject) {
        self.pStorage.setObject("", forKey: "accessToken")
        self.pStorage.synchronize()
        FBSession.activeSession().closeAndClearTokenInformation()
        dispatch_async(dispatch_get_main_queue(), {
            var storyboard = self.storyboard
            var view = storyboard?.instantiateViewControllerWithIdentifier("initialView") as ViewController
            
            self.presentViewController(view, animated: true, completion: nil)
            
            
        })
    }
    
    @IBAction func unwindToStartLand(unwindSegue: UIStoryboardSegue) {
        
    }
    
    /********************************************************************
    *
    * UI MANIPULATION FUNCTIONS.
    * this is where the ui manipulation functions for writing codes are
    * written and places.
    *
    *********************************************************************
    */
}