//
//  ClientAPI.swift
//  OhMyGastos
//
//  Created by ThinkDWM on 4/12/15.
//  Copyright (c) 2015 ThinkDWM. All rights reserved.
//

import Foundation

public class ClientAPI {
    
    // for development
//    var API_URL = "http://172.16.2.100:8000"
    // for testing deployment
    var API_URL = "http://omg.steakrewards.com"

    var CLIENT_ID = "5a50b2d157c6babaf845"
    var CLIENT_SECRET = "71d5d06263f83df522a14b8136ebf9d3e49d9a62"
    var GRANT_TYPE = "password"
    
    
    public func connectSocialMedia(data: NSDictionary, completion: (response: AnyObject)->Void!) {
        /*
        * email
        * username
        * avatar
        * gender
        * social_id (to know what it is)
        * social_media (whether facebook or google)
        */
        let email = data.objectForKey("email") as NSString
        let avatar = data.objectForKey("picture") as NSString
        let gender = data.objectForKey("gender") as NSString
        let socialId = data.objectForKey("id") as NSString
        let firstName = data.objectForKey("first_name") as NSString
        let lastName = data.objectForKey("last_name") as NSString
        let socialMedia = data.objectForKey("social_media") as NSString
        var charGender = gender == "male" ? "M":"F"
        var params = "email=" + email + "&username=" + email + "&avatar=" + avatar + "&gender=" + charGender
        params += "&social_id=" + socialId + "&social_media=" + socialMedia + "&first_name=" + firstName + "&last_name=" + lastName
        request(API_URL + "/users/social-media/connect/", method: "POST", data: params, isAuthenticated: false, completion: {(response: AnyObject) -> Void in
            completion(response: response)
        })
    }
    
    public func getHousehold(completion: (response: AnyObject)->Void!) {
        request(API_URL + "/household/", method: "GET", data: "", isAuthenticated: true, completion: {(response: AnyObject) -> Void in
            completion(response: response)
        })
    }
    
    public func getExpenses(completion: (response: AnyObject)->Void!) {
        request(API_URL + "/household/expenses/", method: "GET", data: "", isAuthenticated: true, completion: {(response: AnyObject) -> Void in
            completion(response: response)
        })
    }
    
    public func createExpense(name: NSString, expense: NSString, image: NSString, completion: (response: AnyObject)->Void!) {
        var params = "name=" + name + "&expense=" + expense + "&image=" + image
        request(API_URL + "/household/expense/", method: "POST", data: params, isAuthenticated: true, completion: {(response: AnyObject) -> Void in
            completion(response: response)
        })
    }
    
    private func request(urlString: NSString, method: NSString, data: NSString, isAuthenticated: Bool, completion: ((response: AnyObject)->Void)!) {
        // This is where the request function lies It can cater POST and GET requests. (so far)
        
        var request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        
        request.HTTPMethod = method
        
        if (method == "POST") {
            var err: NSError?
            request.HTTPBody = data.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
        if (isAuthenticated) {
            var accessToken = NSUserDefaults.standardUserDefaults().objectForKey("accessToken") as NSString
            request.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        }
        
        var queue: NSOperationQueue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler: { (response: NSURLResponse!, responseData: NSData!, error: NSError!) -> Void in
            var newError: NSError?
            if let thisResponse = response as? NSHTTPURLResponse {
                if (thisResponse.statusCode == 500) {
                    completion(response: NSDictionary())
                } else {
                    if let jsonData: AnyObject = NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableContainers, error: &newError) {
                        completion(response: jsonData)
                    } else {
                        completion(response: NSDictionary())
                    }
                }
            }
        })
    }
    
}

extension String {
    var md5ify: String! {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        
        CC_MD5(str!, strLen, result)
        
        var hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.dealloc(digestLen)
        
        return String(format: hash)
    }
}