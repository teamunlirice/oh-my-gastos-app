//
//  ViewController.swift
//  OhMyGastos
//
//  Created by ThinkDWM on 4/11/15.
//  Copyright (c) 2015 ThinkDWM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    /********************************************************************
    *
    * INITIALIZATION
    * this is where the initialization takes place.
    * view did load and other view did functions should be placed in
    * here.
    *
    *********************************************************************
    */
    let pStorage = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if var accessToken = self.pStorage.objectForKey("accessToken") as? NSString {
            if accessToken != "" {
                dispatch_async(dispatch_get_main_queue(), {
                    self.performSegueWithIdentifier("startLanding", sender: self)
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /********************************************************************
    *
    * ACTION FUNCTIONS.
    * this is where the action functions for writing codes are written
    * and places.
    *
    *********************************************************************
    */
    @IBAction func connectWithFacebookAction(sender: AnyObject) {
        // if the session state is any of the two "open" states when the button is clicked.
        
        if (FBSession.activeSession().state == FBSessionState.Open || FBSession.activeSession().state == FBSessionState.OpenTokenExtended) {
            // close the session and remove the access token from the cache
            // the session state handler (in the app delegate) will be called automatically
            FBSession.activeSession().closeAndClearTokenInformation()
        } else {
            // open a session showing the user the login UI
            // you must ALWAYS ask for public_profile permissions when opening a session
            // on our case we will be asking for the email as well as the user_friends
            FBSession.openActiveSessionWithReadPermissions(["public_profile", "user_friends", "email"], allowLoginUI: true, completionHandler: { (session: FBSession!, state: FBSessionState, error: NSError!) -> Void in
                self.sessionStateChanged(session, state: state, error: error)
            })
        }
    }
    
    /********************************************************************
    *
    * UI MANIPULATION FUNCTIONS.
    * this is where the ui manipulation functions for writing codes are written
    * and places.
    *
    *********************************************************************
    */
    
    func sessionStateChanged(session: FBSession!, state: FBSessionState, error: NSError!) {
        
        if ((error == nil) && state == FBSessionState.Open) {
            let mutableDict = NSMutableDictionary()
            FBRequestConnection.startWithGraphPath("/me/", completionHandler: { (connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
                let resultDict = result as NSDictionary
                mutableDict.addEntriesFromDictionary(resultDict)
                FBRequestConnection.startWithGraphPath("/me/picture?redirect=0&height=500&type=normal&width=500", completionHandler: { (picConnection: FBRequestConnection!, picResult: AnyObject!, picError: NSError!) -> Void in
                    let picDict = picResult as NSDictionary
                    let picUrl = picDict.objectForKey("data")?.objectForKey("url") as NSString
                    mutableDict.addEntriesFromDictionary(NSDictionary(object: picUrl, forKey: "picture"))
                    mutableDict.addEntriesFromDictionary(NSDictionary(object: "facebook", forKey: "social_media"))
                    var client = ClientAPI()
                    client.connectSocialMedia(mutableDict, completion: { (accessToken) -> Void in
                        self.showScreen(accessToken as NSDictionary, from: "facebook")
                    })
                })
            })
            return
        } else {
            
        }
        
        if (state == FBSessionState.Closed || state == FBSessionState.ClosedLoginFailed) {
            // if the session is closed
        }
        
        FBSession.activeSession().closeAndClearTokenInformation()
    }
    
    func showScreen(data: NSDictionary, from: NSString) {
        self.pStorage.setObject(data.objectForKey("access_token"), forKey: "accessToken")
        self.pStorage.synchronize()
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("startLanding", sender: self)
        })
    }
    
}


extension String {
    var md5: String! {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        
        CC_MD5(str!, strLen, result)
        
        var hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.dealloc(digestLen)
        
        return String(format: hash)
    }
}
